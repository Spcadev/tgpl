// Dup2 prints the count and text of lines that appear more than once
// in the input.  It reads from stdin or from a list of named files.
package main

import (
	"bufio"
	"fmt"
	"os"
)

// Duplicates hold the file type name associated with the duplicated entry.
type Duplicates struct {
	text string
	file string
}

func main() {
	counts := make(map[Duplicates]int)
	files := os.Args[1:]
	if len(files) == 0 {
		countLines(os.Stdin, counts)
	} else {
		for _, arg := range files {
			f, err := os.Open(arg)
			if err != nil {
				fmt.Fprintf(os.Stderr, "dup2: %v\n", err)
				continue
			}
			countLines(f, counts)
			f.Close()
		}
	}
	for line, n := range counts {
		if n > 1 {
			fmt.Printf("%d\tDuplicate: '%s'\tFile: '%s'\n", n, line.text, line.file)
		}
	}
}

func countLines(f *os.File, counts map[Duplicates]int) {
	input := bufio.NewScanner(f)
	for input.Scan() {
		counts[Duplicates{text: input.Text(), file: f.Name()}]++
	}
	// NOTE: ignoring potential errors from input.Err()
}
